## MiniX 4
![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX4/miniX4.gif)
### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX4/)


[Repository](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX4/miniX4.js)

#### Coerced consent
The sketch coerced consent has the primary focus of user tracking online and the difficulties of trying to avoid it. Every time you visit a new website today (at least within the EU), you’re faced with choosing whether or not you want to allow cookies being saved, as well as whether you’re ok with data being saved for analytics, advertising, etcetera. An attempt to avoid cookies, e.g., by using a private browsing function will result in you being asked this same question every time you visit the site. The result is a near forced consent to cookies and analytics, as the inconvenience is sure to drive the vast majority of users into consenting to these. The sketch tries to convey this tendency by assuming, and then simply not allowing the user to cancel their consent to having their data captured. 

The program featured something new in the form capturing the video from the webcam and using it in the sketch. However, it proved to be quite simple, as it could simply be treated as an image when being inserted and manipulated in the code. What proved very useful in my code was the AND (```&&```) operator for the ```if-else``` statements, as it helped me make the red X that will always evade the user trying to click on it. It may have been possible with simpler syntax than mine, but it still proved a learning experience doing it this way. I did attempt to make use of the ```mouseOver``` syntax, but something wasn’t working as I expected, and I couldn’t get it to work as I intended. However, as it did work exactly as I intended it to, I’ll still count that as a success, even though it’s not the most efficient way to do it. 


Something like a video feed of a persons face being used to collect data isn’t far fetched. AI has come a long way, and even in a fairly simple bit of code you can run a facetracker, which may be able to decipher emotions or other data from the changes in distances between the points on the same persons face. The  fear of being surveilled also leads to many people covering their webcams (which has been fairly common to do even while data capture was far less extensive than it is today). The intent was also to make the user a bit frantic in realizing that their video is being captured, and also that there’s a seemingly simple way of avoiding it, only for it to turn out to be difficult/impossible to do, hinting at the sometimes quite deceptive practices used by websites to make you consent to cookies and data collection. 
