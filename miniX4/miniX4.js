let capture;
let img;
let imgx;
let imgy;

function preload() {
img = loadImage('disagree.png');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();
  textAlign(CENTER,CENTER);
  textSize(24);
  imageMode(CENTER);
  imgx=200
  imgy=200

}

function draw() {
  background(65);
  image(capture, width/2,height/2, 640, 480);
  image(img, imgx, imgy, 50, 50)
  if(mouseX<imgx+30 && mouseX>imgx-30 && mouseY>imgy-30 && mouseY<imgy+30){
    imgx=random(25,windowWidth-25);
    imgy=random(25,windowHeight-25);
    }
  fill(255);
  text("To not have your data collected and sold, please press the X", windowWidth/2,50);
  }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
