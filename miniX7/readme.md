## MiniX 7
![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX7/miniX7.gif)
### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX7/)

[Repository](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX7/miniX7.js)

I’ve chosen to rework my miniX4, Capture All. It doesn’t feature many changes per se, but mostly additions to the impression of the project. The addition of stressing the capture of the video feed with a classic blinking “REC”-sign in order to alert the user to it happening is done with a gif with a transparent background. 

I also implemented a countdown timer, which starts at 10, the variable of which controls actions during the countdown. This featured my first usage of the remainder operator (```%```) and the variable for timer decreasing every time the remainder of frameCount/60 is equal to 0. As the timer decreases, various text signaling that certain data is being retrieved from the user’s device appears in the top left of the screen. The idea of the project has changed slightly in the fact that it’s clearer to me how little of a stretch it is to have the mentioned data collected from practically any online service that has access to it. For that reason, it’s now more of a project to visualize this extraction and make it visible to the user, who are otherwise likely oblivious to the extent of the collection.


>”Political aesthetics refers back to the critical theory of the Frankfurt School, particularly to the ideas of Theodor Adorno and Walter Benjamin, that enforce the concept that cultural production — which would now naturally include programming — must be seen in a social context.” - 

As Shoshana Zuboff touches on in the VPRO documentary about surveillance capitalism, the big tech companies have been running rampant in this sector for almost two decades – only recently with expansion of GDPR and new information coming to light about the extent of data collection, the distribution of it, and its potential uses, has the public eye begun to open to the potential dangers. Visualizations like this project may help further this advancement. 

>"We feel that it is important to further explore the intersections of technical and conceptual aspects of code in order to reflect deeply on the pervasiveness of computational culture and its social and political effects — from the language of human-machine languages to abstraction of objects, datafication and recent developments in automated machine intelligence, for example."

This quote on its own almost perfectly sums up the idea behind my project (in light of Zuboff's critique), as it it recognizes the intertwinement of data-collecting services and medias tight integration into our social lives, and the potential political implications of having this profile available for such a large part of the population.
