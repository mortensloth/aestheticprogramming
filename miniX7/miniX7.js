let capture;
let img;
let imgx;
let imgy;
let timer=10;
let recgif;

function preload() {
img = loadImage('disagree.png');
recgif=loadImage('rec.gif');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();
  textAlign(CENTER,CENTER);
  textSize(24);
  imageMode(CENTER);
  imgx=200
  imgy=200

}

function draw() {
  background(65);
  image(capture, width/2,height/2, 640, 480);
  image(img, imgx, imgy, 50, 50)
  if(mouseX<imgx+30 && mouseX>imgx-30 && mouseY>imgy-30 && mouseY<imgy+30){
    imgx=random(25,windowWidth-25);
    imgy=random(25,windowHeight-25);
    }
  fill(255);
  text("To not have your data collected and sold, please press the X before the timer runs out", windowWidth/2,25);
  push();
  textSize(48);
  text(timer,windowWidth/2,70);
  pop();
  if(frameCount % 60 == 0 && timer > 0){
    timer--;
    }
    push();
    textAlign(LEFT, TOP);
    textSize(14);
  if(timer<10){
      text("RETRIEVED USERS DEVICE OS",5,15)
      }
  if(timer<8){
      text("RETRIEVED USERS IP-ADDRESS",5,30)
      }
  if(timer<6){
      text("RETRIEVED USERS CREDIT CARD INFORMATION",5,45)
      }
  if(timer<5){
      text("RETRIEVED USERS SEARCH HISTORY",5,60)
      }
  if(timer<4){
      text("RETRIEVED USERS INSTALLED PROGRAMS",5,75)
      }
  if(timer<2){
      text("RETRIEVED USERS FAVORITE FOOD",5,90)
      }
  if(timer<1){
      text("RETRIEVED USERS MOTHERS NAME",5,105)
      }
    pop();
    if(timer==0){
      text("Thank you for your consent",windowWidth/2,windowHeight-25)
      image(recgif,(windowWidth/2)-250,(windowHeight/2)-190,150,150)
      }
    }



function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
