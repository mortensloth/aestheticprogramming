# Geometric emojis

[Executable (cookie)](https://mortensloth.gitlab.io/aestheticprogramming/miniX2/indexcookie.html)

[Repository(cookie)](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX2/cookie.js)

[Executable (tortoise)](https://mortensloth.gitlab.io/aestheticprogramming/miniX2/indextortoise.html)

[Executable (tortoise)](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX2/tortoise.js)

![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX2/minix2_cookie.JPG)


![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX2/minix2_tortoise.JPG)


## Description of emojis and their code

My 2 emojis are a cookie (eating a smaller cookie) and a tortoise with a blank expression.
There's not much to unpack in terms of the emojis themselves, as  they're both relatively simple and completely static, but the cookie uses some syntax that i haven't used before, such as ```scale();```, which caused some issues when using an indexed variable for the larger cookies X-and Y-coordinates for placement of later elements. I think i solved it, since I used ```scale(0.2);``` for making the smaller cookie, by following it up with ```scale(5);``` in order to return to "normal". It also offered a great learning experience for keeping track of things like ```stroke();```
and ```noStroke();```, ```fill();``` and ```noFill();``` and making sure to "cancel out" when i was switching back and forth for the different elements, which applied to the uses of ```translate();``` as well, which i used almost excessively when segmenting the shell of the tortoise.
Using ```curve();``` to draw various parts of my emojis also turned out to be a bigger challenge than expected, as it takes a minute to adjust and realize how the control points help shape the curve.

## Reflections on representation
Seeing as Femke Snelting in "Modifying the universal" explains how unicode assigns no real value or personalization of emojis, and that it all happens client-side. Thereby part of the blame is shifted onto the manufacturers who have made their interpretations of these emojis.
However, speaking from a personal experience of the supposed target recipient of representation, I've never viewed the classic yellow emojis as a representation of myself, but merely as an expression of emotion. However, seeing as anyones personal experience is as valid as my own, I've still formulated a suggestion to go along with my own emoji-designs. Based on the idea of emojis merely as an expression of emotion, and the fact that much of the emoji-fication of the unicode happens client-side, each user should maybe be able to set their own preference and apply different sets of emojis to their own device. A user would then have the same range of emotions and situations to send with their messages, but would always see themselves (or a cookie, or a tortoise, or whatever they want) in the situation depicted by the emoji.
