let cookieX = 300
let cookieY = 300
function setup() {
createCanvas(windowWidth,windowHeight);

}
function draw() {
noStroke();
fill(191, 140, 57);
ellipse(cookieX,cookieY,200,200); //cookie

ellipse(cookieX+75,cookieY-60,20,20);
ellipse(cookieX-60,cookieY-73,20,20);
ellipse(cookieX+95,cookieY,20,20);
ellipse(cookieX+75,cookieY+60,20,20);
ellipse(cookieX-10,cookieY+93,20,20);
ellipse(cookieX-75,cookieY-60,20,20);
ellipse(cookieX+5,cookieY-95,20,20);
ellipse(cookieX-92,cookieY+25,20,20);
ellipse(cookieX-70,cookieY+67,20,20);
ellipse(cookieX+47,cookieY+85,20,20);
ellipse(cookieX-95,cookieY-25,20,20);
// ^cookie texture
fill(51,31,1);
ellipse(cookieX+50,cookieY+50,12,12);
ellipse(cookieX+75,cookieY+15,12,12);
ellipse(cookieX-75,cookieY-40,12,12);
ellipse(cookieX-60,cookieY+10,12,12);
ellipse(cookieX-10,cookieY+80,12,12);
ellipse(cookieX+12,cookieY+40,12,12);
ellipse(cookieX+35,cookieY-40,12,12);
ellipse(cookieX-10,cookieY-70,12,12);
ellipse(cookieX-50,cookieY+75,12,12);
ellipse(cookieX+30,cookieY+10,12,12);
ellipse(cookieX-10,cookieY+10,12,12);
ellipse(cookieX+80,cookieY-40,12,12);
//^chocolate chips


fill(255);
stroke(0);
ellipse(cookieX-60,cookieY-20,40,20); //left eye
ellipse(cookieX-10,cookieY-20,40,20); // right eye
fill(0);
ellipse(cookieX-70,cookieY-20,5,5) //left pupil
ellipse(cookieX,cookieY-20,5,5) //right pupil
noFill();
stroke(0);
curve(150,150,cookieX-70,cookieY+40,cookieX-20,cookieY+40,200,400); //mouth
curve(cookieX-80,cookieY+20,cookieX-75,cookieY+43,cookieX-68,cookieY+33,cookieX-70,cookieY-10); //left corner of mouth
translate(8,-5);
curve(cookieX+50,cookieY-36,cookieX-25,cookieY+36,cookieX-15,cookieY+45,cookieX-20,cookieY+40); //right corner of mouth
translate(-8,5);
scale(0.2);
noStroke();
translate(cookieX+200,cookieY+1300);
fill(191, 140, 57);
ellipse(cookieX,cookieY,200,200);
ellipse(cookieX+75,cookieY-60,20,20);
ellipse(cookieX-60,cookieY-73,20,20);
ellipse(cookieX+95,cookieY,20,20);
ellipse(cookieX+75,cookieY+60,20,20);
ellipse(cookieX-10,cookieY+93,20,20);
ellipse(cookieX-75,cookieY-60,20,20);
ellipse(cookieX+5,cookieY-95,20,20);
ellipse(cookieX-92,cookieY+25,20,20);
ellipse(cookieX-70,cookieY+67,20,20);
ellipse(cookieX+47,cookieY+85,20,20);
ellipse(cookieX-95,cookieY-25,20,20);
translate(cookieX-200,cookieY-1300);
// ^mini cookie and casual cannibalism
fill(51,31,1);
translate(-100,1000)
ellipse(cookieX+50,cookieY+50,12,12);
ellipse(cookieX+75,cookieY+15,12,12);
ellipse(cookieX-75,cookieY-40,12,12);
ellipse(cookieX-60,cookieY+10,12,12);
ellipse(cookieX-10,cookieY+80,12,12);
ellipse(cookieX+12,cookieY+40,12,12);
ellipse(cookieX+35,cookieY-40,12,12);
ellipse(cookieX-10,cookieY-70,12,12);
ellipse(cookieX-50,cookieY+75,12,12);
ellipse(cookieX+30,cookieY+10,12,12);
ellipse(cookieX-10,cookieY+10,12,12);
ellipse(cookieX+80,cookieY-40,12,12);
translate(100,-1000)
fill(255);
scale(5);
stroke(255);
ellipse(55,245,25,25); //missing chunk from mini cookie


}
