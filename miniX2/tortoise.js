let bodyX = 300 //x-coordinate of body
let bodyY = 300 //y-coordinate of body
let footXadj = 80 //distance from body to foot on x-axis
let footYadj = 90 //distance from body to foot on y-axis
let shellcolor = (148, 103, 35);

function setup() {
createCanvas (windowWidth, windowHeight);

}

function draw() {
  fill(32, 112, 0);
  noStroke();
  rect(275,165,50,20); //neck
  fill(32, 112, 0);
  ellipse(bodyX+footXadj,bodyY-footYadj,50,50); //left front foot
  ellipse(bodyX-footXadj,bodyY-footYadj,50,50); //right front foot
  ellipse(bodyX+footXadj,bodyY+footYadj,50,50); //left back foot
  ellipse(bodyX-footXadj,bodyY+footYadj,50,50); //right back foot
  noStroke();
  ellipse(bodyX,bodyY,200,250); //Body of tortoise
  stroke(shellcolor);
  fill(171, 134, 79);
  ellipse(bodyX,bodyY,180,225); //Shell detail
  //Shell segment mayhem incoming
  noFill();
  strokeWeight(2);
  stroke(shellcolor);
  beginShape();
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();

  translate(0,40);
  beginShape();
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(0,-40);

  translate(40,20);
  beginShape();
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(-40,-20);

  translate(-40,20);
  beginShape();
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(40,-20);

  translate(40,-20);
  beginShape();
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(-40,20);

  translate(-40,-20);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(40,20);

  translate(0,-40);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(0,40);

  translate (0,-80);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(0,80);

  translate(0,80);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(0,-80);

  translate(-40,60);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(40,-60);

  translate(40,60);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(-40,-60);

  translate(-40,-60);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(40,60);

  translate(40,-60);
  vertex(bodyX-25,bodyY);
  vertex(bodyX-15,bodyY-20);
  vertex(bodyX+15,bodyY-20);
  vertex(bodyX+25,bodyY);
  vertex(bodyX+15,bodyY+20);
  vertex(bodyX-15,bodyY+20);
  vertex(bodyX-25,bodyY);
  endShape();
  translate(-40,60);
  //End of shell segment mayhem
  noStroke();
  fill(32, 112, 0);
  ellipse(bodyX,bodyY-190,45,15) //mouth
  rect(260,110,80,65,20,20,20,20); //head

  fill(0);
  ellipse(bodyX-15,bodyY-170,10,10); //left eye
  ellipse(bodyX+15,bodyY-170,10,10); //right eye
}
