## MiniX 5
![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX5/miniX5.gif)
### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX5/)


[Repository](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX5/miniX5.js)

The code itself for my auto-generator is very simple and only takes up 22 lines of code. This was somewhat by design, as I did want to see how brief you could make a bit of code that could still be considered an auto-generator. The code does utilize the ```random();``` syntax to achieve this effect, but I’m still quite satisfied with the result. The program draws white ellipses (with an alpha value of 75) at an increasing distance from the center of the canvas, and at a random rotation in a circle around the center. The distance is achieved with a conditional statement, and the distance from the center is limited to 300 to speed up the pattern-generation of the program, and the placement on around the perimeter of the current distance is achieved with the aforementioned usage of ```random();```.  


Over time the program creates varying patterns that emerge from the center (where the ellipses overlap faster due to there being less space for them to occupy on a smaller radius around the center). Upon being given enough time to run, the program would fill the center of the canvas with a white ellipse with a height and width of 300 pixels, but during the process it draws various patterns, typically reminiscent of a flower or “arms” reaching out from the center.


These parallels are drawn to words that stem from living organisms, and seem fit to describe the development of a program that can take many different forms, almost as if it itself is alive. However, in my own opinion it’s simply automating the process that I’ve asked it to perform. There is however the slight issue of randomness. In this domain, the computer is far superior. If you ask for random, you get random, while with a person, there’s almost always going to be some sort of underlying pattern. Some may strive for consistent intervals between values, or others may think that just 2 values in close proximity warrants leaping to the opposite end of the scale. In reality, random should never take these into account. It may result in 3, 4, 100 practically identical values in a row, but that’s just the name of the game.
