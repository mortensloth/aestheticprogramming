## MiniX 1

Morten Sloth Enemærke

### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX1/)


[Repository](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX1/miniX1.js)

#### Short description
My executable consists of a smaller ellipse orbiting on a larger ellipse. Centered in the large ellipse there is a counter that increases by 1 every second. Additionally, the smaller ellipse completes 1 orbit every second. 

![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX1/miniX1_gif.gif)
#### Syntax

First off is the larger ellipse, which is placed in the center of the canvas by using ```width/2``` and ```height/2``` for the x- and y-coordinates.
Then the counter, which consists of text which is first centered with ```textAlign(CENTER, CENTER)```, and the inserter text uses ```round(millis()/1000``` in order to produce a second-counter.

The smaller ellipse (or rather the movement) was a slightly more difficult project - to achieve the movement along the larger ellipse i used the ```translate()``` function - but simply inserting constants would achieve a static ellipse. I found inspiration in the p5js.org references and found that using ```p5.Vector.fromAngle(millis()``` would let the smaller ellipse run along the larger ellipse. However, since the fromAngle function uses radians, it would take the ellipse 2𝝅, or around 6,3 seconds to run a full orbit. Since i wanted it to take 1 second, i had to convert it from radians to degrees. Because the ```millis()``` counts milliseconds from when ```setup()``` was started, the ```millis()/500*PI``` ensures the correct timing for the orbit.

#### Limitations

During the conding process i wanted to include another "arm" in the form or a rectangle or triangle for showing minutes, but struggled with getting the ```translate()``` to work as intended, as the first instance of the syntax was being applied to both the ellipse and the additional figure and preventing me from differentiating in the time it took each of them to orbit the large ellipse. 
One issue i ran into and didn't quite have the time to fix was the fact that upon refreshing the page, running the code from the start, the smaller ellipse would start on the right side of the larger ellipse due to the '120' value in my ```translate()``` function. While trying to edit this to have it start on the top of the larger ellipse to better ressemble an analog clock, it would create more issues, eg. no longer running. On top of that, for the second-counter i decided to use ```millis()``` again to ensure that the counter and the clock would run on the same time, as ```second()``` and ```minute()``` use the current time on the device the code is running on, rather than the time elapsed since ```setup()``` was run, as it is for ```millis()```. I then had to use ```round()``` (as well as devide by 1000) to avoid having way too many numbers on the screen. Unfortunately this meant that as soon as the number reached 0.5000 it would round up to 1, hence the ellipse starting on the right side-edge and the counter increasing by 1 every time it passes the left side-edge.

#### Impressions
The concept for the clock came first as quite a meta idea that started while spending a while trying to figure out what i wanted to produce with my code. The end-product has quite a simplistic expression in terms of simple figures (albeit to me quite advanced movements affecting them), and a simple counter. 
The context further affected the process in terms of the daylight waning outside as i progressed with my coding, which inspired the rather dark color-scheme.
Before beginning the course i was already familiar with pseudo-code, and why it's useful. This being one of my first coding experiences, the actual coding experience is very different, mainly due to the strict structure required for the code to be able to run. 
