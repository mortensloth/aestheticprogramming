function setup() {
}

function draw() {
  createCanvas (600,600);
  background(25,32,138);
  fill(0);
  ellipse(width/2,height/2,240,240);
  textAlign(CENTER, CENTER);
  fill(256);
  textSize(32);
  text(round(millis()/1000),300,300);
  translate(p5.Vector.fromAngle(millis()/500*PI,120));
  fill(204,204,0);
  ellipse(width/2,height/2,25,25);

}
