let willSize = {
  w:86,
  h:89
};
let will;
let chrisimg;
let willPosY;
let mini_height;
let min_chris = 7;//min chris on the screen
let chris = [];
let score =0, lose = 0;
let keyColor = 45;


  function preload(){
  will = loadImage("willsmith.png");
  chrisimg = loadImage("chrisrock.png");
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  willPosY = height/2;
  mini_height = height/2;
}
function draw() {
  background(255);
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkchrisNum(); //available chris
  showchris();
  image(will, 0, willPosY, willSize.w, willSize.h);
  checkslap(); //scoring
  checkResult();
}

function checkchrisNum() {
  if (chris.length < min_chris) {
    chris.push(new Chris());
  }
}

function showchris(){
  for (let i = 0; i <chris.length; i++) {
    chris[i].move();
    chris[i].show();

  }
}

function checkslap() {
  //calculate the distance between chris'
  for (let i = 0; i < chris.length; i++) {
    let d = int(
      dist(willSize.w/2, willPosY+willSize.h/2,
        chris[i].pos.x, chris[i].pos.y)
      );
    if (d < willSize.w/1.4) { //distance at which to slap chris
      score++;
      chris.splice(i,1);
    }else if (chris[i].pos.x < -5) { //will missed chris
      lose++;
      chris.splice(i,1);
    }
  }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('You have slapped '+ score + " Chris Rock's", 10, height/1.4);
    text('You have missed ' + lose + " Chris Rock's", 10, height/1.4+20);
    fill(keyColor,255);
    text('PRESS the ARROW UP & DOWN key to slap Chris Rock',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    text("Jada's name wasn't kept out of his f****** mouth", width/3, height/1.4);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    willPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    willPosY+=50;
  }
  //reset if will moves out of range
  if (willPosY > mini_height) {
    willPosY = mini_height;
  } else if (willPosY < 0 - willSize.w/2) {
    willPosY = 0;
  }

  // Prevents default browser behaviour
  return false;
}
