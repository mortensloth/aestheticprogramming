## MiniX 6
![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX6/miniX6.gif)
### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX6/)

#### Repositories:
[Class](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX6/chrisclass.js)

[Sketch](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX6/slapsketch.js)

My game has taken inspiration from current events – Will Smith slapping Chris Rock at the Academy Awards. It features Chris Rock (programmed as a class, shown with the help of a .png-file) spawning at the right-hand side of the screen and moving towards the left-hand side of the screen, where a mid-slap Will Smith is waiting, whose vertical position is controlled by the players up and down arrow keys. The object of the game for the player to move Will into position to slap Chris Rock as their paths cross, before he moves out of the frame and is missed.  If you’ve missed more than you’ve slapped, and you’ve missed more than 2, you lose the game. 


As mentioned, Chris is programmed in a class – the constructor initializes the object, and assigns a speed at which he moves and declares it’s position. The speed is a random value between 2 and 5, and a given instance of the object’s x-coordinate is reduced by this value each frame. The object is then shown with an image at it’s position with a static width and height.

Abstraction is an important concept within object-oriented programming, as the point of classes. for example, is to encapsule the key characteristics and defining features of the given object/class. This leads to having to choose/evaluate what these features are, and hence what will be possible to do with the given class. 

My game, while seemingly light-hearted (however light-hearted you consider violence to be), still does raise an important point. The game having the objective of repeating (and in the process, poking fun at) the slap, raises the issue of society’s elite hardly living the by same rules and laws as the rest of us. The fact that someone resorted to violence, with millions of witnesses, as a reaction to a joke, then sat back down, is being omitted, but is not a far-fetched thought when trying to slap Chris over and over again.
