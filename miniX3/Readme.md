## MiniX3

### Links
[Executable](https://mortensloth.gitlab.io/aestheticprogramming/miniX3/)


[Repository](https://gitlab.com/mortensloth/aestheticprogramming/-/blob/main/miniX3/miniX3.js)

![Alt Text](https://gitlab.com/mortensloth/aestheticprogramming/-/raw/main/miniX3/miniX3.gif)

In my eyes one of the most important characteristics is being aesthetically pleasing – a pleasant passing of time while operations (the process being unimportant to the end result the user is waiting for) are being performed in the background, while reassuring the user that something IS indeed happening.

My own most common experiences with throbbers are probably in terms of downloading content, sometimes files, but mostly for streaming services. One of the core elements of this is the repetition of the same “action”, files being transferred, and I wanted to convey this cycle in my throbber. Therefore, the throbber reaches its “starting point” in its rotation again after just a few seconds, in terms of the arcs “meeting up” on the same part of the circles that they’re drawn from all at the same time. I wanted it to look this way by design, but arriving at it, I have to admit, involved a bit of luck. In the beginning I merely set a variable responsible for the rotation of the arcs to increase by 0.05 per frame. 

The whole idea for the multiple arcs was to have them rotate at different speeds, which was achieved by multiplying the rotation by larger factors, increasing in consistent increments from the largest to the smallest arc. The time comes into play sort of “organically” in the code itself in the sense that the only element of time involved is based on the speed at which the operations of the code are performed with help from utilizing the framerate to perform the rotation.

As mentioned previously, the most common occurrence of throbbers in my daily life is present in streaming video-content, often on Netflix. This one is a bit of an odd case in my opinion when comparing to similar services’ throbbers. Netflix’ features a percentage in the center of the throbber which indicates how far it’s come with loading the content, however it seems to do more harm than good, as it’ll very often quickly make its way to 99%, until halting on the last percent for longer than it has spent on the first 99. Just like Netflix, many services today are heavily characterized by being on-demand. Bank transfers can be done instantaneously. A lot of humans also have a quite limited attention span when it comes to interacting with technology, and the throbber probably tries to grab at the impatience of the user and assure them that the device is working on the request.

In order to get the correct placement of the arcs, as well as the different rotation speeds, I stumbled a bit with the combination of the two, and had to make very ineffective use of the ```push();``` and ```pop();``` syntax. This miniX did however teach me more about how to work the ```rotate``` syntax, as I initially tried to create my throbber with curves, which made them rotate around the origin rather than as intended.
