let rotation = 0;
//throbber
function setup() {
 //create a drawing canvas
 createCanvas(windowWidth, windowHeight);
}

function draw() {
rotation = rotation+0.05
  background(0)
  noFill();

  push();
  stroke(247, 0, 255);
  strokeWeight(2.5);
  translate(windowWidth/2,windowHeight/2);
  rotate(rotation);
  arc(0,0,80,80,2*PI-0.8,2*PI+0.8);
  pop();

  push();
  stroke(0, 207, 45);
  strokeWeight(2.5);
  translate(windowWidth/2,windowHeight/2);
  rotate(rotation*1.5);
  arc(0,0,65,65,2*PI-0.8,2*PI+0.8);
  pop();
  

  push();
  stroke(0, 43, 232);
  strokeWeight(2.5);
  translate(windowWidth/2,windowHeight/2);
  rotate(rotation*2);
  arc(0,0,50,50,2*PI-0.8,2*PI+0.8);
  pop();

  push();
  stroke(235, 215, 0);
  strokeWeight(2.5);
  translate(windowWidth/2,windowHeight/2);
  rotate(rotation*2.5);
  arc(0,0,35,35,2*PI-0.8,2*PI+0.8);
  pop();



}


function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
